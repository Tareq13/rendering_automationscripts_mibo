package smi.rendering.test.pages;

import static org.testng.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import smi.rendering.test.pages.HomePage;

import org.apache.logging.log4j.Logger;
import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.base.DriverContext;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;


public class ActivevoicesSynthesisRequestsPage extends BasePage {

	private static boolean enableReportTestlink;
	public static ReportTestLink rpTestLink;
	private static String chosenProject1="";
	private static String accountName1="";
	private static String chosendomain1="";


	
public void getReady(ExtentTest test, Logger logger,String accountName,String chosenProject,String chosenDomain, Connection con, boolean isThereMoreThanOneAssignedDomain) throws InterruptedException, SQLException, FileNotFoundException {
	
		RandomText generateInput = new RandomText();
		DriverContext.browser.GoToUrl(Setting.AUT_URL);
		DriverContext.browser.Maximize();
		String usernameLogin = ExcelUltility.ReadCell("Email",1);

		// Login only once then run the following tests
		List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
		CurrentPage= GetInstance(HomePage.class);
//		CurrentPage.As(HomePage.class).isHomepageReady();

		System.out.print(accountName);
		// Get account,project and domain with the requested language and logged-in email
		Random random = new Random();
		int index = random.nextInt(account_project_domain.size());
		LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,"System","System","Call Center");
		//LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,accountName,chosenProject,chosenDomain);
		chosenProject1=chosenProject ;
		accountName1=accountName ;
		chosendomain1=chosenDomain;
		
		
		// Select random voice and domain
		Thread.sleep(2000);
		CurrentPage= GetInstance(HomePage.class);
		String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//		String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);	
		LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);
		
		// Click clear text first
		Thread.sleep(1000);
		CurrentPage.As(HomePage.class).clearText.click();
				
		//Thread.sleep(3000);
	
		// Enter text in the text box
		String randomText = generateInput.getSentencesFromFile();
		LogUltility.log(test, logger, "Text to enter: " + randomText);
		CurrentPage.As(HomePage.class).Textinput.click();
		CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
		
		
	//	DriverContext.browser.Maximize();
		
		// Implicit wait
		DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


	}

public void getReadyForRepeat(ExtentTest test, String server, Logger logger, String voice, String domain, boolean isThereMoreThanOneAssignedDomain) throws InterruptedException, SQLException, FileNotFoundException {
	
	//DriverContext.browser.GoToUrl(server);
	// Login only once then run the following tests
	
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);
	
	LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,accountName1, chosenProject1, chosendomain1);

	CurrentPage= GetInstance(HomePage.class);
	CurrentPage.As(HomePage.class).isHomepageReady();
	
	// Select random voice and domain
	//String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
	// Select random voice and domain
	Thread.sleep(2000);
	CurrentPage= GetInstance(HomePage.class);
	String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//	String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//	LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);	
	LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);
	

	//String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
	//LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);
	// Select the random domain
//	CurrentPage.As(HomePage.class).cmbDomain.click();
//	Thread.sleep(1000);
//	CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbDomain, domain);
//	LogUltility.log(test, logger, "Chosen Voice: " + voice + ", Chosen Domain: " + domain);

	// Click clear text first
	CurrentPage.As(HomePage.class).clearText.click();
	
	// Enter text in the text box
	RandomText generateInput = new RandomText();
	String randomText = generateInput.getSentencesFromFile();
	LogUltility.log(test, logger, "Text to enter: " + randomText);
	CurrentPage.As(HomePage.class).Textinput.click();
	CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
	
}


public void getReadyForRepeatDuplicate(ExtentTest test, Logger logger, boolean isThereMoreThanOneAssignedDomain) throws InterruptedException, SQLException, FileNotFoundException {
	
	//DriverContext.browser.GoToUrl(Setting.AUT_URL);
	// Login only once then run the following tests
	
	//LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);
	
	//LoginUtilities.selectSpecificProjectDomain(GetInstance(SelectPage.class), logger, "Bank Hapoalim Project", "Bank Hapoalim Domain");
	
	CurrentPage= GetInstance(HomePage.class);
	CurrentPage.As(HomePage.class).isHomepageReady();
	
	// Select random voice and domain
	//String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
	// Select random voice and domain
	Thread.sleep(2000);
	CurrentPage= GetInstance(HomePage.class);
	String chosenVoice = CurrentPage.As(HomePage.class).chooseRandomVoice();
//	String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
//	LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice + ", Chosen Domain: " + chosenDomain);	
	LogUltility.log(test, logger, "Chosen Voice: " + chosenVoice);
	

	// Click clear text first
	CurrentPage.As(HomePage.class).clearText.click();
	
	// Enter text in the text box
	RandomText generateInput = new RandomText();
	String randomText = generateInput.getSentencesFromFile();
	LogUltility.log(test, logger, "Text to enter: " + randomText);
	CurrentPage.As(HomePage.class).Textinput.click();
	CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
	
}

	public void synthisizeText(ExtentTest test, Logger logger) throws InterruptedException {

		// Check if the wave bar for clip is displayed
		//Thread.sleep(10000);
		CurrentPage= GetInstance(HomePage.class);
		boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
		LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
		assertTrue(isWaveBarDisplayed);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
}
