package smi.rendering.test.pages;

import com.smi.framework.base.BasePage;
import static org.testng.Assert.assertEquals;
import java.awt.AWTException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ForgetPasswordPage extends BasePage {

    // Email address field
	@FindBy(how=How.NAME,using ="email")
	public WebElement txtUserName;
	
	// Submit button
	@FindBy(how=How.NAME,using ="SubmitButton")
	public WebElement btnSubmit;
	
//	// Submit button
//	@FindBy(how=How.ID,using ="SubmitButtonLogin")
//	public WebElement btnSubmit;
	
	// Title of the page : Speech Rendering App
	@FindBy(how=How.ID,using ="sub_title")
	public WebElement title;
	
	// Login link
	@FindBy(how=How.LINK_TEXT,using ="Login")
	public WebElement LoginLink;
	
	//Message :  You have not given a correct e-mail address 
	@FindBy(how=How.XPATH,using ="html/body/div[1]/div[2]/form/div[1]/span")
	public WebElement WrongEmailMessage;
	
	// Message: A password reset message was sent to your email address. Please check your inbox.
	@FindBy(how=How.XPATH,using ="/html[1]/body[1]/div[1]/h1[1]")
	public WebElement SuccessMessage;
	
	// Message: You have not given a correct e-mail address
	@FindBy(how=How.XPATH,using ="/html/body/div/div[3]/form/div[1]/span")
	public WebElement ErrorMessage;
		
	
	/**
	 * Fill username and click the submit button
	 * @param username
	 */
	public void ForgetPassword(String username)
	{	
		txtUserName.sendKeys(username);
		btnSubmit.click();
	}
	
	/**
	 * Wait for the success message
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	public void successMessageCheck(Logger logger, String expectedMessage) throws InterruptedException, AWTException {
		int timesToTry = 50;
		CurrentPage = GetInstance(ForgetPasswordPage.class);
		String doneMessage;
		while (timesToTry != 0)
		{
			try {
				    doneMessage = CurrentPage.As(ForgetPasswordPage.class).SuccessMessage.getText();
					assertEquals(doneMessage, expectedMessage);
					logger.info("Get the system confirmation message: " + doneMessage);
					return;				
			} catch (Exception e) {
				timesToTry--;
				Thread.sleep(100);
			}
		}
	}
	
}

