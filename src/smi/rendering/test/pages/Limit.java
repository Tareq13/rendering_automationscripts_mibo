package smi.rendering.test.pages;

public class Limit {
	private String account_id;
	private int hour;
	private int max_concurrent;
	private int max_this_hour;
	
	
	public String getId() {
	    return account_id;
	}
	public void setId(String account_id) {
	    this.account_id = account_id;
	}
	public int getHour() {
	    return hour;
	}
	public void setHour(int hour) {
	    this.hour = hour;
	}
	public int getMax_concurrent() {
	    return max_concurrent;
	}
	public void setMax_concurrent(int max_concurrent) {
	    this.max_concurrent = max_concurrent;
	}
	public int getMax_this_hour() {
	    return max_this_hour;
	}
	public void setMax_this_hour(int max_this_hour) {
	    this.max_this_hour = max_this_hour;
	}
}
