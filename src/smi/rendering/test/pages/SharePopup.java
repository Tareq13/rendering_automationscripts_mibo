package smi.rendering.test.pages;


import java.awt.AWTException;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.LogUltility;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Define elements and methods for the Share popup window
 *
 */
public class SharePopup extends BasePage {

    // Share popup header
	@FindBy(how=How.XPATH,using ="//*[@id='sharePopup']/div[1]")
	public WebElement ShareTitle;
	
	// Share popup
	@FindBy(how=How.ID,using ="sharePopup")
	public WebElement SharePopup;
	
	// People field
	@FindBy(how=How.ID,using ="shareRcpt")
	public WebElement People;
	
	// Notes textbox  
	@FindBy(how=How.ID,using ="shareNotes")
	public WebElement Notes;
	
	// Cancel button
	@FindBy(how=How.CLASS_NAME,using ="popupCancel")
	public WebElement CancelBtn;
	
	// Share button
	@FindBy(how=How.CLASS_NAME,using ="popupDoShare")
	public WebElement ShareBtn;
	
	// View dropdown
	@FindBy(how=How.XPATH,using ="//*[@id='sharePopup']/div[2]/div[1]/div[2]/div/div[2]/p")
	public WebElement ViewOptions;
	
	// empty People field message : Please add recipients     X
	@FindBy(how=How.XPATH,using ="html/body/div[7]/div/div[2]/div/span")
	public WebElement Recipients; 
	
	// Message for sharing successfully : Text Shared Successfully     X 
	@FindBy(how=How.XPATH,using ="html/body/div[7]/div/div[2]/div/span")
	public WebElement TextShared;
	
	/**
	 * Share file
	 * @param username -  fill in the people field
	 */
    public void Share_File(String username){
    	
    	People.sendKeys(username);
    	ShareBtn.click();
//    	CurrentPage= GetInstance(HomePage.class);
    }
    
    /**
     * Get the submitted Notes info from the database
     * @param con
     * @param emails
     * @param notes
     * @return
     * @throws SQLException
     */
    public List<List<String>> getNotesValues(Connection con, String emails, String notes) throws SQLException {

    	notes = notes.replaceAll("'", "''");
    	String query = "select notes,list_of_emails from renderer_text_notes Where list_of_emails = '" + emails + "' AND notes = '" + notes + "'";
    	   ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
    	      
    	      // Use metdata in order to fetch columns names
    	      ResultSetMetaData rsmd = resultSet.getMetaData();

    	      // Number of columns
    	      int columnsNumber = rsmd.getColumnCount();

    	      // To save the fetched rows
    	      List<List<String>> table = new ArrayList<List<String>>();
    	      List<String> columnNames = new ArrayList<String>();
    	      List<String> namesRow = new ArrayList<String>();
    	      
    	      // Get the columns names
    	         for (int i = 1; i <= columnsNumber; i++) {
    	          columnNames.add(rsmd.getColumnName(i));
    	         }

    	      while (resultSet.next()) {
    	          for (int i = 1; i <= columnsNumber; i++) {
    	           namesRow.add(resultSet.getString(i));
    	          }
    	      }

    	      table.add(columnNames);
    	      table.add(namesRow);
    	      
    	   return table;
}
    
    
    
    /**
	 * Wait for the save popup
	 * @return
	 * @throws InterruptedException 
	 * @throws AWTException 
	 */
	public String sharePopupCheck(ExtentTest test, Logger logger) throws InterruptedException, AWTException {
		int timesToTry = 50;
		CurrentPage = GetInstance(SharePopup.class);
		String title;
		while (timesToTry != 0)
		{
			System.out.println("timesToTry: " + timesToTry);
			try {
				CurrentPage = GetInstance(SharePopup.class);
				title = CurrentPage.As(SharePopup.class).ShareTitle.getText();
				if (title.equals("Share")) {
				//assertEquals(title, "Share");
				LogUltility.log(test, logger, "Share popup title: " + title);
					return title;
				}
				else {
					timesToTry--;
					Thread.sleep(100);
				}
			} catch (Exception e) {
				timesToTry--;
				Thread.sleep(100);
			}
		}
		return "";
	}
	
	
	
	 /**
     * Check if the note and emalis list saved successfully in the DB
     * @param con
     * @param emails
     * @param notes
     * @return
     * @throws SQLException
     */
    public boolean checkNotesDB(Connection con, String emails, String notes) throws SQLException {
    	
    	notes = notes.replaceAll("'", "''");
    	 //  String query = "select notes,list_of_emails from renderer_text_notes Where list_of_emails = '" + emails + "' AND notes = '" + notes + "'";
    	   String query =    "select exists (select true from renderer_text_notes where list_of_emails = '" + emails +"' AND notes ='" + notes + "')";
    	   ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
    	   resultSet.next(); 
    	 
    	   return resultSet.getBoolean(1);
    	   

    }

	
}
