package smi.rendering.test.pages;


import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import com.smi.framework.base.BasePage;
import com.smi.framework.uiltilies.DatadUltilities;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Define elements and methods for the feedback popup window
 */
public class FeedbackPage extends BasePage {
	
    // Feedback header 
	@FindBy(how=How.XPATH,using ="//*[@id='feedbabkPopup']/div[1]")
	public WebElement Feedbackheader;
	
	// Email label 
	@FindBy(how=How.XPATH,using ="//*[@id='feedbabkPopup']/div[2]/div[1]/label")
	public WebElement Emailheader;
	
	// Text label 
	@FindBy(how=How.XPATH,using ="//*[@id='feedbabkPopup']/div[2]/div[2]/label")
	public WebElement Textheader;
	
	// Feedback Popup
	@FindBy(how=How.CLASS_NAME,using ="mainPopup")
	public WebElement FeedbackPopup;
	
	// Email field
	@FindBy(how=How.ID,using ="feedbackPopupEmail")
	public WebElement Email;
	
	// Text box
	@FindBy(how=How.ID,using ="feedbackPopupText")
	public WebElement Textbox;
	
	// Send button
	@FindBy(how=How.CLASS_NAME,using ="popupFeedback")
	public WebElement SendBtn;
	
	// Cancel button
	@FindBy(how=How.CLASS_NAME,using ="popupCancel")
	public WebElement CancelBtn;
	
	// Message for empty email : Please enter email address     X
	@FindBy(how=How.XPATH,using ="html/body/div[7]/div/div[2]/div")
	public WebElement EmptyEmail;
	
	// Message for invalid email : Please enter a valid email address     X
	@FindBy(how=How.XPATH,using ="html/body/div[7]/div/div[2]/div")
	public WebElement InvalidEmail;
	
	// Message that the feedback sent successfully : Your feedback has been submitted     X
	@FindBy(how=How.XPATH,using ="html/body/div[7]/div/div[2]/div/span") 
	public WebElement SuccessMessage;
	
	// Message that the text is less than 10 chars ( Or empty): Please enter feedback text of at least 10 characters     X
	@FindBy(how=How.XPATH,using ="html/body/div[7]/div/div[2]/div/span")
	public WebElement CharsNumber;
	
	/**
	 * - Send feedback successfully
	 */
    public void Send_Feedback(String randomEmail, String randomText ){
    	this.Email.sendKeys(randomEmail);
    	this.Textbox.sendKeys(randomText);
    	this.SendBtn.click(); 
    }
    
    /**
     * Get feedback values from the feedback table
     * @param con - SQL connection
     * @param email
     * @param text
     * @return
     * @throws SQLException
     */
    public List<List<String>> getFeedbackValues(Connection con, String email, String text) throws SQLException {
    	   // Get the id of the selected age
    		text = text.replaceAll("'", "''");
    	   String query = "select reply_to_email,message from feedback Where reply_to_email = '" + email + "' AND message = '" + text + "'" ;
    	   ResultSet resultSet = DatadUltilities.ExecuteQuery(query, con);
    	      
    	      // Use metdata in order to fetch columns names
    	      ResultSetMetaData rsmd = resultSet.getMetaData();

    	      // Number of columns
    	      int columnsNumber = rsmd.getColumnCount();

    	      // To save the fetched rows
    	      List<List<String>> table = new ArrayList<List<String>>();
    	      List<String> columnNames = new ArrayList<String>();
    	      List<String> namesRow = new ArrayList<String>();
    	      
    	      // Get the columns names
    	         for (int i = 1; i <= columnsNumber; i++) {
    	          columnNames.add(rsmd.getColumnName(i));
    	         }

    	      // Get the distinct age values
    	      while (resultSet.next()) {
    	          for (int i = 1; i <= columnsNumber; i++) {
    	           namesRow.add(resultSet.getString(i));
    	          }
    	      }

    	      table.add(columnNames);
    	      table.add(namesRow);
    	      
    	   return table;
    	  }
    
    }
