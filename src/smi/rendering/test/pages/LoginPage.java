package smi.rendering.test.pages;

import com.smi.framework.base.BasePage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * - Defined the elements
 *
 */
public class LoginPage extends BasePage {

    // Email field
	@FindBy(how=How.NAME,using ="user_name")
	public WebElement txtUserName;
	
	// Password field
	@FindBy(how=How.NAME,using ="password")
	public WebElement txtPassword;

	// Submit button
	@FindBy(how=How.NAME,using ="SubmitButton")
	public WebElement btnLogin;
	
	// Page title
	@FindBy(how=How.ID,using ="sub_title")
	public WebElement title;
	
	// Not signedup link
	@FindBy(how=How.LINK_TEXT,using ="Not signed up yet?")
	public WebElement NotSigned;
	
	// Forgot password link
	@FindBy(how=How.LINK_TEXT,using ="Forgot password?")
	public WebElement ForgetPassword;
	
	// The version text
	@FindBy(how=How.CLASS_NAME,using ="other-auth")
	public WebElement Version;
	
	// Message: Username or password is wrong.  
	@FindBy(how=How.XPATH,using ="/html[1]/body[1]/div[1]/div[3]/form[1]/div[1]/span[1]")
	public WebElement WrongInput;
	
	// Speechmorphing logo
	@FindBy(how=How.XPATH,using ="html>body>header>img")
	public WebElement logo;
	
	// Message for blank password  
	@FindBy(how=How.XPATH,using ="/html[1]/body[1]/div[1]/div[3]/form[1]/div[2]/span[1]")
	public WebElement BlankPassword;
	
	// Message for blank email   
	@FindBy(how=How.XPATH,using ="/html[1]/body[1]/div[1]/div[3]/form[1]/div[1]/span[1]")
	public WebElement BlankEmail;
	
	// Message: Account is not yet active. Check your email for activation instructions   
	
	@FindBy(how=How.XPATH,using ="/html[1]/body[1]/div[1]/div[3]/form[1]/div[1]/span[1]")
	public WebElement ActivationMessage;
	
	/**
	 * Login with valid username and password then get the home page instance back
	 * @param username
	 * @param password
	 * @return
	 */
	public HomePage Login(String username,String password)
	{	
		// Fill username and password
		txtUserName.sendKeys(username);
		txtPassword.sendKeys(password);
		
		// Click the login submit button
		btnLogin.click();
		
		return GetInstance(HomePage.class);
	}
	
	/**
	 * Verify that the element displayed in the page
	 * @param element - element name
	 * @return
	 */
	public boolean FindElement(String element) {
	    try {
	     switch (element) 
	     {
	             case "txtUserName":
	            	 return this.txtUserName.isDisplayed();
	             case "txtPassword":
	            	 return this.txtPassword.isDisplayed();
	             case "btnLogin":
	            	 return this.btnLogin.isDisplayed();
		         case "NotSigned":
		        	 return this.NotSigned.isDisplayed();
		         case "ForgetPassword":
				      return this.ForgetPassword.isDisplayed();
	     }
	         } catch (NoSuchElementException e) {
	             return false;
	         }
	    return false;
	   }
	
}
