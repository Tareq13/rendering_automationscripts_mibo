
package smi.rendering.test.pages;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.aventstack.extentreports.ExtentTest;
import com.smi.framework.base.BasePage;
import com.smi.framework.uiltilies.LogUltility;

/**
 * - Defined the elements and methods for the open popup
 *
 */
public class Say_As_Popup extends BasePage {
	
	// Say As text field
	@FindBy(how=How.ID,using ="ipaPopupText")
	public WebElement txtInput;
	
	// OK button  popupIpaSet
	//@FindBy(how=How.XPATH,using ="//*[@id='ipaPopup']/div[3]/a[2]")
	@FindBy(how=How.CLASS_NAME,using ="popupIpaSet")
	public WebElement btnOK;
	
	// Pronunciation Symbol radio button
	@FindBy(how=How.ID,using ="ipaPopupTypeIpa")
	public WebElement rdoPunctiation;
	
	// Vowels group
	@FindBy(how=How.XPATH,using ="//*[@id='ipaPopup']/div[2]/div[3]/div")
	public WebElement btnVowels;
	
	// Consonant group
	@FindBy(how=How.XPATH,using ="//*[@id='ipaPopup']/div[2]/div[4]/div/i[1]")
	public WebElement btnConsonant;
	
	// Stress group
	@FindBy(how=How.XPATH,using ="//*[@id='ipaPopup']/div[2]/div[5]/div/i[1]")
	public WebElement btnStress;
	
	// Lenghening group
	@FindBy(how=How.XPATH,using ="//*[@id='ipaPopup']/div[2]/div[6]/div/i")
	public WebElement btnLenghening;
	
	// Says as window title
	@FindBy(how=How.XPATH,using ="//*[@id='ipaPopup']/div[1]")
	public WebElement titleSayAs;
	
	// Vowel in Pronunciation symbols title
	@FindBy(how=How.XPATH,using="//*[@id=\\\\\\\"ipaPopup\\\\\\\"]/div[2]/div[3]")
	public WebElement vowelTitle;
	
	// Consonant in pronunciation symbols title
	@FindBy(how=How.XPATH,using="//*[@id=\"ipaPopup\"]/div[2]/div[4]")
	public WebElement consonantTitle;
	
	// Cancel button in say-as popup
	@FindBy(how=How.XPATH,using="//*[@id=\"ipaPopup\"]/div[3]/a[1]")
	public WebElement btnCancel;
	//-------------------------------------------------
	// VOWELS
	/**
	 * Choose random vowel
	 * @return
	 * @throws AWTException
	 */
	public String chooseRandomVowel() throws AWTException {
		// Get the available vowels
		List<String> vowelsvalues = this.getVowelsValues();
		
		// Choose a random vowel
		Random randomizer = new Random();
		String randomVowel = vowelsvalues.get(randomizer.nextInt(vowelsvalues.size()));
		
		// Select the random vowel
		String Chosen = this.SelectFromVowels(randomVowel);
		
		return Chosen;
	}
	
	/**
	 * Get Vowels Values
	 * @return
	 * @throws AWTException
	 */
	public ArrayList<String> getVowelsValues() throws AWTException {
	
        // Get all of the options
        List<WebElement> options = btnVowels.findElements(By.xpath("//*[@id='ipaPopup']/div[2]/div[3]/div/i"));
        
        ArrayList<String> vowels= new ArrayList<String>();
        for (WebElement opt : options) {
        	if(!opt.getText().isEmpty())
        		vowels.add(opt.getText());
        }
        return vowels;
}
	/**
	 * Select a vowel
	 * @param option
	 * @return
	 */
	public String SelectFromVowels(String option) {
		 // Get all of the options
		List<WebElement> options = btnVowels.findElements(By.xpath("//*[@id='ipaPopup']/div[2]/div[3]/div/i"));

		// Loop through the options and select the one that matches
				    for (WebElement opt : options) {
				        if (opt.getText().equals(option)) {
				            opt.click();
				            return option;
		        }
		    }
					return null;
	}
	
	//-------------------------------------------------
	// CONSONANT
	/**
	 * Choose random consonant
	 * @return
	 * @throws AWTException
	 */
	public String chooseRandomConsonant() throws AWTException {
		// Get the available consonants
		List<String> consonantvalues = this.getConsonantValues();
		
		// Choose a random consonant
		Random randomizer = new Random();
		String randomConsonant = consonantvalues.get(randomizer.nextInt(consonantvalues.size()));
		
		// Select the random consonant
		String Chosen = this.SelectFromConsonants(randomConsonant);
		
		return Chosen;
	}
	
	/**
	 * Get Vowels Values
	 * @return
	 * @throws AWTException
	 */
	public ArrayList<String> getConsonantValues() throws AWTException {
	
        // Get all of the options
        List<WebElement> options = btnConsonant.findElements(By.xpath("//*[@id='ipaPopup']/div[2]/div[4]/div/i"));
        
        ArrayList<String> consonants= new ArrayList<String>();
        for (WebElement opt : options) {
        	if(!opt.getText().isEmpty())
        		consonants.add(opt.getText());
        }
        return consonants;
}
	/**
	 * Select a vowel
	 * @param option
	 * @return
	 */
	public String SelectFromConsonants(String option) {
		 // Get all of the options
		List<WebElement> options = btnConsonant.findElements(By.xpath("//*[@id='ipaPopup']/div[2]/div[4]/div/i"));

		// Loop through the options and select the one that matches
				    for (WebElement opt : options) {
				        if (opt.getText().equals(option)) {
				        	System.out.println("CHECK THISS: "+opt.getText());
				            opt.click();
				            return option;
		        }
		    }
					return null;
	}
	
	//-------------------------------------------------
		// Stress
		/**
		 * Choose random stress
		 * @return
		 * @throws AWTException
		 */
		public String chooseRandomStress() throws AWTException {
			// Get the available stresses
			List<String> stressValues = this.getStressValues();
			
			// Choose a random stress
			Random randomizer = new Random();
			String randomStress = stressValues.get(randomizer.nextInt(stressValues.size()));
			
			// Select the random stress
			String Chosen = this.SelectFromStress(randomStress);
			
			return Chosen;
		}
		
		/**
		 * Get Stresses Values
		 * @return
		 * @throws AWTException
		 */
		public ArrayList<String> getStressValues() throws AWTException {
		
	        // Get all of the options
	        List<WebElement> options = btnStress.findElements(By.xpath("//*[@id='ipaPopup']/div[2]/div[5]/div/i"));
	        
	        ArrayList<String> consonants= new ArrayList<String>();
	        for (WebElement opt : options) {
	        	consonants.add(opt.getText());
	        }
	        return consonants;
	}
		/**
		 * Select a stress
		 * @param option
		 * @return
		 */
		public String SelectFromStress(String option) {
			 // Get all of the options
			List<WebElement> options = btnStress.findElements(By.xpath("//*[@id='ipaPopup']/div[2]/div[5]/div/i"));

			// Loop through the options and select the one that matches
					    for (WebElement opt : options) {
					        if (opt.getText().equals(option)) {
					            opt.click();
					            return option;
			        }
			    }
						return null;
		}
		
		//-------------------------------------------------
				// Lenghening
				/**
				 * Choose random lenghening
				 * @return
				 * @throws AWTException
				 */
				public String chooseRandomLenghening() throws AWTException {
					// Get the available lenghening
					List<String> lengheningValues = this.getLengheningValues();
					
					// Choose a random lenghening
					Random randomizer = new Random();
					String randomLenghening = lengheningValues.get(randomizer.nextInt(lengheningValues.size()));
					
					// Select the random lenghening
					String Chosen = this.SelectFromLenghening(randomLenghening);
					
					return Chosen;
				}
				
				/**
				 * Get lenghening Values
				 * @return
				 * @throws AWTException
				 */
				public ArrayList<String> getLengheningValues() throws AWTException {
				
			        // Get all of the options
			        List<WebElement> options = btnLenghening.findElements(By.xpath("//*[@id='ipaPopup']/div[2]/div[6]/div/i"));
			        
			        ArrayList<String> lenghenings= new ArrayList<String>();
			        for (WebElement opt : options) {
			        	lenghenings.add(opt.getText());
			        }
			        return lenghenings;
			}
				/**
				 * Select a lenghening
				 * @param option
				 * @return
				 */
				public String SelectFromLenghening(String option) {
					 // Get all of the options
					List<WebElement> options = btnLenghening.findElements(By.xpath("//*[@id='ipaPopup']/div[2]/div[6]/div/i"));

					// Loop through the options and select the one that matches
							    for (WebElement opt : options) {
							        if (opt.getText().equals(option)) {
							            opt.click();
							            return option;
					        }
					    }
								return null;
				}
				
				/**
				 * Check say as pop up is opened
				 * @return 
				 * @throws InterruptedException 
				 * @throws AWTException 
				 */
				public void isSayAsWindowOpen() throws InterruptedException {
					int timesToTry = 10;
					CurrentPage = GetInstance(Say_As_Popup.class);
					while (timesToTry != 0)
					{
						try {
							String title = CurrentPage.As(Say_As_Popup.class).titleSayAs.getText();
							boolean result = title.contains("Say-As");
							assertTrue(result);
								return;				
						} catch (Exception e) {
							timesToTry--;
							System.out.println(timesToTry);
							Thread.sleep(100);
						}
					}
				}
				
				/**
				 * Check say as pop up is opened and return an answer
				 * @return 
				 * @throws InterruptedException 
				 * @throws AWTException 
				 */
				public boolean isSayAsWindowOpenWithAnswer() throws InterruptedException {
					int timesToTry = 5;
					CurrentPage = GetInstance(Say_As_Popup.class);
					while (timesToTry != 0)
					{
						try {
							String title = CurrentPage.As(Say_As_Popup.class).titleSayAs.getText();
							boolean result = title.contains("Say-As");
							if (result == true || false) {
								assertTrue(result);
									return true;		
							}
							else {
								timesToTry--;
								System.out.println(timesToTry);
								Thread.sleep(100);
							}
						} catch (Exception e) {
							timesToTry--;
							System.out.println(timesToTry);
							Thread.sleep(100);
						}
					}
					return false;
				}
				
				
	
}

