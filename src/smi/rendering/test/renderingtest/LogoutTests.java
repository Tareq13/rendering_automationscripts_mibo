package smi.rendering.test.renderingtest;

import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import jxl.read.biff.BiffException;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.SelectPage;

/**
* All tests in logout folder
*
*/
public class LogoutTests extends FrameworkInitialize{

	private static Connection con;
	private static Logger logger = null;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	public boolean isThereMoreThanOneAssignedDomain;
	
	/**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException {
	
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
	
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("Logout", Setting.Browser);
		
	// Logger
	logger = LogManager.getLogger(LogoutTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite= LogoutTests ");
	logger.info("Logout Tests - FrameworkInitilize");
	
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Check if we have only one domain within one project
	isThereMoreThanOneAssignedDomain =   GetInstance(HomePage.class).As(HomePage.class).isThereMoreThanOneAssignedDomain(con);
	}

	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	 public void beforeTest(Method method) throws Exception 
	{  
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}

	/**
	 * SRA-18:Verify user can logout successfully
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA18_User_can_logout_successfully() throws InterruptedException, SQLException
	{   
		test = extent.createTest("LogoutTests-SRA-18", "Verify user can logout successfully");
		
		// Login and check that user logged in successfully
		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);

		if (isThereMoreThanOneAssignedDomain) {
			// Select random project and domain
			LoginUtilities.selectRandomProjectDomain(GetInstance(SelectPage.class), logger, con);
		}
		// Click the logout link 
		logger.info("Pressing the logout link");
		CurrentPage= GetInstance(HomePage.class);
    	CurrentPage.As(HomePage.class).lnkLogout.click();
    	//Thread.sleep(10000);
		
    	// Verify that user is logged out and forwarded to the login page
    	CurrentPage= GetInstance(LoginPage.class);
    	logger.info("Check for the elements in the login page");
    	
    	// Verify that the username field appear in the login page
    	boolean findUserName = CurrentPage.As(LoginPage.class).FindElement("txtUserName");
    	logger.info("Username field: " + findUserName); 
    	Assert.assertTrue(findUserName);
    	
    	// Verify that the password field appear in the login page
    	boolean findPassword = CurrentPage.As(LoginPage.class).FindElement("txtPassword");
    	logger.info("Password field: " + findPassword);
    	Assert.assertTrue(findPassword);
    	
    	// Verify that the submit button appear in the login page
    	boolean findSubmit = CurrentPage.As(LoginPage.class).FindElement("btnLogin");
    	logger.info("Submit button: " + findSubmit);
    	Assert.assertTrue(findSubmit);
    	
    	// Verify that the Not signed up link appear in the login page  
    	boolean findSignLink = CurrentPage.As(LoginPage.class).FindElement("NotSigned");
    	logger.info("Not signed up Link: " + findSignLink);
    	Assert.assertTrue(findSignLink);
    	
    	// Verify that the forget password link appear in the login page 
    	boolean findForgotLink = CurrentPage.As(LoginPage.class).FindElement("ForgetPassword");
    	logger.info("Forget password link: " + findForgotLink);
    	Assert.assertTrue(findForgotLink); 
    	
    	LogUltility.log(test, logger, "Test Case PASSED");
}
	
	
	/**
	 * SRA-219:Verify user can logout from project/domain page successfully
	 * @throws InterruptedException
	 * @throws SQLException 
	 */
	@Test (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA219_Verify_user_can_logout_from_project_domain_page_successfully() throws InterruptedException, SQLException
	{   
		test = extent.createTest("LogoutTests-SRA-219", "Verify user can logout from project/domain page successfully");
		
		// Login and check that user logged in successfully
		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);
		
		// Click the logout link 
		logger.info("Pressing the logout link");
		CurrentPage= GetInstance(HomePage.class);
    	CurrentPage.As(HomePage.class).lnkLogout.click();
    	//Thread.sleep(10000);
		
    	// Verify that user is logged out and forwarded to the login page
    	CurrentPage= GetInstance(LoginPage.class);
    	logger.info("Check for the elements in the login page");
    	
    	// Verify that the username field appear in the login page
    	boolean findUserName = CurrentPage.As(LoginPage.class).FindElement("txtUserName");
    	logger.info("Username field: " + findUserName); 
    	Assert.assertTrue(findUserName);
    	
    	// Verify that the password field appear in the login page
    	boolean findPassword = CurrentPage.As(LoginPage.class).FindElement("txtPassword");
    	logger.info("Password field: " + findPassword);
    	Assert.assertTrue(findPassword);
    	
    	// Verify that the submit button appear in the login page
    	boolean findSubmit = CurrentPage.As(LoginPage.class).FindElement("btnLogin");
    	logger.info("Submit button: " + findSubmit);
    	Assert.assertTrue(findSubmit);
    	
    	// Verify that the Not signed up link appear in the login page  
    	boolean findSignLink = CurrentPage.As(LoginPage.class).FindElement("NotSigned");
    	logger.info("Not signed up Link: " + findSignLink);
    	Assert.assertTrue(findSignLink);
    	
    	// Verify that the forget password link appear in the login page 
    	boolean findForgotLink = CurrentPage.As(LoginPage.class).FindElement("ForgetPassword");
    	logger.info("Forget password link: " + findForgotLink);
    	Assert.assertTrue(findForgotLink); 
    	
    	LogUltility.log(test, logger, "Test Case PASSED");
}

	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    {

		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Logout",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
		// Refresh the website for the new test
		DriverContext._Driver.navigate().refresh();
    	}
	
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {
			 
			 DriverContext._Driver.quit();
	    }
}