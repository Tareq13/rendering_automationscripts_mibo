package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.concurrent.TimeUnit;

import jxl.read.biff.BiffException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.SelectPage;
import smi.rendering.test.pages.SignupPage;

import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;

/**
 * All tests in Login folder 
 *
 */
public class LoginTests extends FrameworkInitialize{
	
	private static Connection con;
	private static Logger logger = null;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	public boolean isThereMoreThanOneAssignedDomain;
	
    /**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
     * @throws ClassNotFoundException 
	 */
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException {
	
	freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);

	ConfigReader.GetAllConfigVariable();
	
	// Initiate a report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("Login", Setting.Browser);
		
	// Logger
	logger = LogManager.getLogger(LoginTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite= LoginTests ");
	logger.info("Login Tests - FrameworkInitilize");
	
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);

	CurrentPage= GetInstance(LoginPage.class);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
	generateInput = new RandomText();
	
	// Check if we have only one domain within one project
//	isThereMoreThanOneAssignedDomain =   GetInstance(HomePage.class).As(HomePage.class).isThereMoreThanOneAssignedDomain(con);
	isThereMoreThanOneAssignedDomain = true;
	}
		
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeTest(Method method) throws Exception {
		logger.info("");
		logger.info("#####################################################");
		logger.info("Starting Test: " + method.getName());
		
		//Thread.sleep(10000);
		
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
	}
	
	/**
	 * SRA 1: User can login with exist account successfully
	 * @throws InterruptedException
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA1_User_can_login_with_exist_account_successfully() throws InterruptedException
	{	
		test = extent.createTest("LoginTests-SRA 1", "User can login with exist account successfully");
		logger.info("Login with valid username and password"); 
		DriverContext.browser.GoToUrl(Setting.AUT_URL);
		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, isThereMoreThanOneAssignedDomain);
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * SRA-93:Verify user can�t login with invalid fields
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA93_Verify_that_user_cant_login_with_invalid_inputs() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("LoginTests-SRA-93", "Verify user can�t login with invalid fields");
		// Login with invalid info
		String lang = Setting.Language ;
		Setting.Language = "6"; 
		String username= generateInput.RandomString(5, "letters");
		String password= generateInput.RandomString(5, "letters");
		Setting.Language = lang;
		LogUltility.log(test, logger, "Login with invalid info- username: " + username + ", password: " + password);
		CurrentPage.As(LoginPage.class).Login(username,password);
		//Thread.sleep(2000);
		
		// Check the error message
		String wrongMessage = CurrentPage.As(LoginPage.class).WrongInput.getText();
		LogUltility.log(test, logger, "Error message: " + wrongMessage);
//		assertEquals(wrongMessage, "Username or password is wrong.");
		assertEquals(wrongMessage, "Error: invalid email address.password too short.");
		
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
    /**
     * SRA-127:Verify user cannot login when account activation is not done // email= Activatio@mailinator.com
     * @throws InterruptedException
     * @throws FileNotFoundException 
     */
    @Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA127_Verify_that_user_cant_login_with_not_activated_email() throws InterruptedException, FileNotFoundException
	{
    	test = extent.createTest("LoginTests-SRA-127", "Verify user cannot login when account activation");
    	
    	DriverContext.browser.GoToUrl(Setting.AUT_URL+"/signup.html");
    	CurrentPage= GetInstance(SignupPage.class);
    	
    	// Signup for new activate but don't activate it
    	String lang = Setting.Language ;
    	Setting.Language = "6"; 
    	String fname = generateInput.RandomString(5, "letters");
		String lname = generateInput.RandomString(5, "letters");
		String email = generateInput.RandomString(5, "letters") + "@mailinator.com";
		String password = generateInput.RandomString(6, "letters");
		Setting.Language = lang;
		LogUltility.log(test, logger, "Fill the Signup fields - fname: " + fname + ", lname: " + lname + ", email: " + email + ", password: " + password);
		CurrentPage = GetInstance(SignupPage.class);
		Thread.sleep(6000);
		CurrentPage.As(SignupPage.class).Signup(fname, lname, email, password, password);
		
		CurrentPage.As(SignupPage.class).checkSignupSuccessMessage(test, logger);
		
    	// Login with the not activated account
//		String password= generateInput.RandomString(6, "letters");
		LogUltility.log(test, logger, "Login with invalid info- username: " + email + ", password: " + password);
		CurrentPage= GetInstance(LoginPage.class);
		DriverContext.browser.GoToUrl(Setting.AUT_URL);
		CurrentPage.As(LoginPage.class).Login(email, password);
		//Thread.sleep(2000);
		
		// Check the error message
		String wrongMessage = CurrentPage.As(LoginPage.class).ActivationMessage.getText();
		LogUltility.log(test, logger, "Error message: " + wrongMessage);
//    	assertEquals(wrongMessage, "Account is not yet active. Check your email for activation instructions.");
    	assertEquals(wrongMessage, "Error:please activate your account.");
    	LogUltility.log(test, logger, "Test Case PASSED");
	}
    
    /**
     * SRA-2:Verify user can�t login with NOT exist Email
     * @throws InterruptedException
     * @throws FileNotFoundException 
     */
    @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA2_Verify_that_user_cant_login_with_none_exist_email() throws InterruptedException, FileNotFoundException
	{
    	test = extent.createTest("LoginTests-SRA-2", "Verify user can�t login with NOT exist Email");
    	// Login with unregistered email
    	String lang = Setting.Language ;
    	Setting.Language = "6"; 
		String username= generateInput.RandomString(5, "letters") + "@gmail.com";
		String password= generateInput.RandomString(6, "letters");
		Setting.Language = lang;
		LogUltility.log(test, logger, "Login with invalid info- username: " + username + ", password: " + password);
		CurrentPage.As(LoginPage.class).Login(username,password);
		//Thread.sleep(2000);
		
		// Check the error message
		String wrongMessage = CurrentPage.As(LoginPage.class).WrongInput.getText();
		LogUltility.log(test, logger, "Error message: " + wrongMessage);
//    	assertEquals(wrongMessage, "Username or password is wrong.");
		assertEquals(wrongMessage, "Error:user not Found with details.");
		
    	LogUltility.log(test, logger, "Test Case PASSED");
	}
    
    /**
     * SRA-3:Verify user can�t login with invalid Email
     * @throws InterruptedException
     * @throws FileNotFoundException 
     */
    @Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA3_Verify_user_cant_login_with_invalid_Email() throws InterruptedException, FileNotFoundException
	{
    	test = extent.createTest("LoginTests-SRA-3", "Verify_user_cant_login_with_invalid_Email");
    	// Login with unregistered email
    	String lang = Setting.Language ;
    	Setting.Language = "6"; 
		String Email= generateInput.RandomString(5, "letters") + "@gmail.com";
		String password= generateInput.RandomString(6, "letters");
		Setting.Language = lang;
		LogUltility.log(test, logger, "Login with invalid Email: " + Email + ", password: " + password);
		CurrentPage.As(LoginPage.class).Login(Email,password);
		//Thread.sleep(2000);
		
		// Check the error message
		String wrongMessage = CurrentPage.As(LoginPage.class).WrongInput.getText();
		LogUltility.log(test, logger, "Error message: " + wrongMessage);
//    	assertEquals(wrongMessage, "Username or password is wrong.");
		assertEquals(wrongMessage, "Error:user not Found with details.");

    	LogUltility.log(test, logger, "Test Case PASSED");
	}
    
		/**
		 * SRA-4:Verify user can�t login with too short password
		 * @throws InterruptedException
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA4_Verify_user_cant_login_with_too_short_password() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("LoginTests-SRA-4", "Verify_user_cant_login_with_invalid_Email");
			// Login with unregistered email
			String lang = Setting.Language ;
	    	Setting.Language = "6"; 
			String Email= generateInput.RandomString(5, "letters") + "@gmail.com";
			String password= generateInput.RandomString(2, "letters");
			Setting.Language = lang;
			LogUltility.log(test, logger, "Login with too short password: " + Email + ", password: " + password);
			CurrentPage.As(LoginPage.class).Login(Email,password);
			//Thread.sleep(2000);
			
			// Check the error message
			String wrongMessage = CurrentPage.As(LoginPage.class).WrongInput.getText();
			LogUltility.log(test, logger, "Error message: " + wrongMessage);
//			assertEquals(wrongMessage, "Username or password is wrong.");
			assertEquals(wrongMessage, "Error:password too short.");
			
			LogUltility.log(test, logger, "Test Case PASSED");
		}		  
		  
		/**
		 * SRA-91:Verify user can�t login with blank password
		 * @throws InterruptedException
		 * @throws FileNotFoundException 
		 */
		@Test(groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
		public void SRA91_Verify_that_user_cant_login_with_blank_password() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("LoginTests-SRA-91", "Verify user can�t login with blank password");
			// Login with blank password
			String lang = Setting.Language ;
			Setting.Language = "6"; 
			String username= generateInput.RandomString(5, "letters") + "@gmail.com";
			Setting.Language = lang;
			LogUltility.log(test, logger, "Login with invalid info- username: " + username);
			CurrentPage.As(LoginPage.class).Login(username,"");
			//Thread.sleep(2000);
			
			// Check the error message
			String wrongMessage = CurrentPage.As(LoginPage.class).BlankPassword.getText();
			LogUltility.log(test, logger, "Error message: " + wrongMessage);
	    	assertEquals(wrongMessage, "Please enter password");
	    	LogUltility.log(test, logger, "Test Case PASSED");
		}
	
		/**
		 * SRA-90:Verify user can�t login with blank Email
		 * @throws InterruptedException
		 * @throws FileNotFoundException 
		 */
		@Test(groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA90_Verify_that_user_cant_login_with_blank_Username() throws InterruptedException, FileNotFoundException
		{
			test = extent.createTest("LoginTests-SRA-90", "Verify user can�t login with blank Email");
			// Login with blank username
			String lang = Setting.Language ;
			Setting.Language = "6"; 
			String password= generateInput.RandomString(5, "letters");
			Setting.Language = lang;
			LogUltility.log(test, logger, "Login with invalid info- password: " + password);
			CurrentPage.As(LoginPage.class).Login("",password);
			//Thread.sleep(2000);
			
			// Check the error message
			CurrentPage.As(LoginPage.class).BlankEmail.isDisplayed();
			String wrongMessage = CurrentPage.As(LoginPage.class).BlankEmail.getText();
			LogUltility.log(test, logger, "Error message: " + wrongMessage);
	    	assertEquals(wrongMessage, "Please enter username");
	    	LogUltility.log(test, logger, "Test Case PASSED");
	}
		
		@AfterMethod(alwaysRun = true)
	    public void getResult(ITestResult result, Method method) throws IOException
	    {		
			int testcaseID = 0;
			boolean writeToReport = false;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("Login",method.getName());
			System.out.println("tryCount: " + tryCount);
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
				writeToReport = true;
			}
			else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
				extent.removeTest(test);
				}
				else {
					writeToReport = true;
					}
			
			// Write to report
			if (writeToReport) {
				tryCount = 0;

				 // Write to report
				 if(result.getStatus() == ITestResult.FAILURE)
				    {
				        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
				        test.fail(result.getThrowable());
				        String screenShotPath = Screenshot.captureScreenShot();
				        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
				        // Send result to testlink
				        if (enableReportTestlink == true){
				        	try {
				        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
				            	LogUltility.log(test, logger, "Report to testtlink: " + response);
							} catch (Exception e) {
								LogUltility.log(test, logger, "Testlink Error: " + e);
							}
				        	}
				    }
				    else if(result.getStatus() == ITestResult.SUCCESS)
				    {
				        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
				        // Send result to testlink
				        if (enableReportTestlink == true){
				        	try {
				        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
				            	LogUltility.log(test, logger, "Report to testtlink: " + response);
				    		} catch (Exception e) {
				    			LogUltility.log(test, logger, "Testlink Error: " + e);
				    		}
				        	
				        	}
				    }
				    else
				    {
				        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
				        test.skip(result.getThrowable());
				    }
			        // Get console report
			        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
				    extent.flush();
			}
			// Refresh the website for the new test
			DriverContext._Driver.navigate().refresh();
	    	}
		
			/**
			 * Closing the browser after running all the TCs
			 */
			@AfterClass(alwaysRun = true) 
			public void CloseBrowser() {
				 
				 DriverContext._Driver.quit();
		    }
	}
