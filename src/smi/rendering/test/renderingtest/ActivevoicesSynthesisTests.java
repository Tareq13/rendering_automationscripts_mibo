package smi.rendering.test.renderingtest;

	import static org.testng.Assert.assertTrue;
	import static org.testng.Assert.assertEquals;
	import static org.testng.Assert.assertFalse;
import java.time.LocalDateTime;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
	import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

	import org.apache.logging.log4j.LogManager;
	import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.ITestResult;
	import org.testng.annotations.BeforeMethod;
	import org.testng.annotations.AfterClass;
	import org.testng.annotations.AfterMethod;
	import org.testng.annotations.BeforeClass;
	import org.testng.annotations.Parameters;
	import org.testng.annotations.Test;

	import com.aventstack.extentreports.ExtentReports;
	import com.aventstack.extentreports.ExtentTest;
	import com.aventstack.extentreports.Status;
	import com.aventstack.extentreports.markuputils.ExtentColor;
	import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
	import com.smi.framework.base.FrameworkInitialize;
	import com.smi.framework.config.ConfigReader;
	import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
	import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
	import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

	import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
	import jxl.read.biff.BiffException;
import smi.rendering.test.pages.ActivevoicesSynthesisRequestsPage;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.Limit;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.SelectPage;

	/**
	* All tests in Active voices Synthesis Tests folder
	*
	*/
	public class ActivevoicesSynthesisTests extends FrameworkInitialize{
		private static Connection con;
		private static Logger logger = null;
		private static ExtentReports extent;
		private static ExtentTest test;
		public static ReportTestLink rpTestLink;
		private static boolean enableReportTestlink;
		private int tryCount = 0;
		private static RandomText generateInput;
		public boolean isThereMoreThanOneAssignedDomain;
		private String usernameLogin = "";
		private String chosenProject = "";
		private String accountName = "";
		private String chosenDomain="";

		
		/**
		 * Initialize - run once before all tests
		 * @param browser
		 * @throws BiffException
		 * @throws IOException
		 * @throws InterruptedException
		 * @throws ClassNotFoundException 
		 * @throws SQLException 
		 */
		@Parameters({"browser", "testlink"})
		@BeforeClass(alwaysRun = true)
		public void Initialize(String browser, boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException, Exception {
		
			freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
			ConfigReader.GetAllConfigVariable();

		// Initiate a report
		//@SuppressWarnings("unused")
		extent = ReportUtilities.report("ActivevoicesSynthesisRequests", browser);

		// Logger
		logger = LogManager.getLogger(ActivevoicesSynthesisTests.class);
		logger.info("--------------------------------------------------------------------------------------------------------");
		logger.info("Start a new Test suite= Active voices and Synthesis Requests Tests ");
		logger.info("Active voices and Synthesis Requests Tests - FrameworkInitilize");
		
		con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
		logger.info("Connect to DB " + con.toString());
		
		InitializeBrowser(Setting.Browser);
		//DriverContext.browser.GoToUrl(Setting.AUT_URL);
		ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
		usernameLogin = ExcelUltility.ReadCell("Email",1);
//		
//		// Get account,project and domain with the requested language and logged-in email
		List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
//		LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
		Random random = new Random();
		int index = random.nextInt(account_project_domain.size());
//		LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));
		chosenProject = account_project_domain.get(index).get(1);
		accountName = account_project_domain.get(index).get(0);
		chosenDomain=account_project_domain.get(index).get(2);
		accountName="System";
		chosenProject="System";
		chosenDomain="Call Center";
		
		//DriverContext.browser.Maximize();

		
		// Implicit wait
		//DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Report to testlink
		try {
			if (Setting.ReportToTestlink.equals("true") && testlink == true) {
				enableReportTestlink = true;
				rpTestLink=new ReportTestLink();
			}
			else enableReportTestlink = false;
		} catch (Exception e) {
			LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
			enableReportTestlink = false;
		}
		
		// String builder in order to send the steps to testlink
		LogUltility.newSB();
		
		// Get the Records list
		//generateInput = new RandomText();
		
		// Check if we have only one domain within one project
		isThereMoreThanOneAssignedDomain =   GetInstance(HomePage.class).As(HomePage.class).isThereMoreThanOneAssignedDomain(con);
		usernameLogin = ExcelUltility.ReadCell("Email",1);
		}

		/**
		 * Run this before each test
		 * @param method for test information
		 * @throws Exception
		 */
		@BeforeMethod(alwaysRun = true)
		 public void beforeTest(Method method) throws Exception 
		{  
			logger.info("");
			logger.info("#####################################################");
			logger.info("Starting Test: " + method.getName());
			
			// Refresh the website for the new test
			//DriverContext._Driver.navigate().refresh();
			//CurrentPage = GetInstance(HomePage.class);
			CurrentPage = GetInstance(ActivevoicesSynthesisRequestsPage.class);

			//Thread.sleep(10000);
			//CurrentPage.As(HomePage.class).isHomepageReady();
			//CurrentPage.As(ActivevoicesSynthesisRequestsPage.class).getReady(test, logger, con, isThereMoreThanOneAssignedDomain);

			// Reset string builder
			LogUltility.sbClean();
			
			// Count how many a test was processed
			tryCount++;
		}

		/**
		 * SRA-149:Check the max concurrent in parallel for rendering multiple inputs at the same time
		 * @throws InterruptedException
		 * @throws SQLException 
		 * @throws AWTException 
		 */
		@Test (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA149_Check_the_max_concurrent_in_parallel_for_rendering_multiple_inputs_at_the_same_time() throws InterruptedException, SQLException, FileNotFoundException, AWTException
		{   
			test = extent.createTest("ActivevoicesSynthesisRequests-SRA-149", "Check the max concurrent in parallel for rendering multiple inputs at the same time");
			
			
			//get the current hour in Irland server  
			LocalDateTime irland_time= LocalDateTime.now().minusHours(2);
			int hour=irland_time.getHour();

			// get the limit for a specific hour and account_id...
			CurrentPage= GetInstance(HomePage.class);
			String account_id = CurrentPage.As(HomePage.class).getAccountId(con,accountName);
			List<Limit> limits = CurrentPage.As(HomePage.class).Get_limits_account_Values(con, hour,account_id,accountName);
			LogUltility.log(test, logger, "choosen account : "+limits.get(0).getId()+" ,hour :"+limits.get(0).getHour()+" ,Max_concurrent: "+limits.get(0).getMax_concurrent()+" ,Max_this_hour :"+limits.get(0).getMax_this_hour());
			int max_concurrent=	limits.get(0).getMax_concurrent();	
			int max_this_hour=limits.get(0).getMax_this_hour();
			int failed = 0;

			int numberOfRuns = max_concurrent+1;
			 //numberOfRuns=3;
			// Fill the first opened browser
			CurrentPage= GetInstance(ActivevoicesSynthesisRequestsPage.class);
			CurrentPage.As(ActivevoicesSynthesisRequestsPage.class).getReady(test, logger,accountName, chosenProject , chosenDomain, con, true);
			
			if(numberOfRuns>1) {
			// Open the other windows and login
			for (int i = 1; i < numberOfRuns; i++) {
	            Robot robot = new Robot(); 
	             robot.keyPress(KeyEvent.VK_ALT); 
	             robot.keyPress(KeyEvent.VK_D);    
	             Thread.sleep(2000);
	             robot.keyRelease(KeyEvent.VK_ALT);    
	             robot.keyRelease(KeyEvent.VK_D);  
	             Thread.sleep(2000);             
	             robot.keyPress(KeyEvent.VK_ALT); 
	             robot.keyPress(KeyEvent.VK_ENTER);
	             robot.keyRelease(KeyEvent.VK_ALT);    
	             robot.keyRelease(KeyEvent.VK_ENTER);   
				
	             // Open new window
				//((JavascriptExecutor)DriverContext._Driver).executeScript("window.open();");

				// Focus on last opened window
				for (String handle : DriverContext._Driver.getWindowHandles()) {
					DriverContext._Driver.switchTo().window(handle);
					}
		
				CurrentPage= GetInstance(ActivevoicesSynthesisRequestsPage.class);
				CurrentPage.As(ActivevoicesSynthesisRequestsPage.class).getReadyForRepeatDuplicate(test, logger, isThereMoreThanOneAssignedDomain);
			}
			
			
			// Click play in all the buttons
			for (String handle : DriverContext._Driver.getWindowHandles()) {
				DriverContext._Driver.switchTo().window(handle);
				
				// Click the play button
				CurrentPage.As(HomePage.class).btnplay.click();
				
				}
			
			// Focus on last opened window
			for (String handle : DriverContext._Driver.getWindowHandles()) {
				DriverContext._Driver.switchTo().window(handle);
				}
			
//			// Check for error synthiszing messages
//			for (String handle : DriverContext._Driver.getWindowHandles()) {
//				DriverContext._Driver.switchTo().window(handle);
//				
				// Check the messages
				try {
					//CurrentPage.As(HomePage.class).fadedPopup(logger, "you have reached maximum parallel request.  try again in a little bit.");
					
					CurrentPage.As(HomePage.class).isHomepageReady();
					String message = CurrentPage.As(HomePage.class).displayedMessage.getText();
					boolean MaxLimit=message.equals("you have reached maximum parallel request. try again in a little bit.     X");
					if (!message.isEmpty() && message.equals("you have reached maximum parallel request. try again in a little bit.     X"))
						failed ++;
			    	assertTrue(MaxLimit);
				} catch (Exception e) {
					System.out.println(e);
				}
								
//				}
			
			}
				
			int result = numberOfRuns-failed;
			LogUltility.log(test, logger, "Succeded to synthesize: " + result);
	}
		
		/**
		 * SRA-147:Check the max request in specific hour for rendering  inputs at the same hour
		 * @throws InterruptedException
		 * @throws SQLException 
		 * @throws AWTException 
		 */
		@Test (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA147_Check_the_max_request_in_specific_hour_for_rendering_inputs_at_the_same_hour() throws InterruptedException, SQLException, FileNotFoundException, AWTException
		{   
			test = extent.createTest("ActivevoicesSynthesisRequests-SRA-147", "Check the max request in specific hour for rendering  inputs at the same hour");
			
			
			//get the current hour in Irland server  
			LocalDateTime irland_time= LocalDateTime.now().minusHours(2);
			int hour=irland_time.getHour();

			// get the limit for a specific hour and account_id...
			CurrentPage= GetInstance(HomePage.class);
			String account_id = CurrentPage.As(HomePage.class).getAccountId(con,accountName);
			List<Limit> limits = CurrentPage.As(HomePage.class).Get_limits_account_Values(con, hour,account_id,accountName);
			LogUltility.log(test, logger, "choosen account : "+limits.get(0).getId()+" ,hour :"+limits.get(0).getHour()+" ,Max_concurrent: "+limits.get(0).getMax_concurrent()+" ,Max_this_hour :"+limits.get(0).getMax_this_hour());
			int max_concurrent=	limits.get(0).getMax_concurrent();	
			int max_this_hour=limits.get(0).getMax_this_hour();
			
			int numberOfRuns = max_this_hour+1;
			//numberOfRuns=3;
			// Fill the first opened browser
			CurrentPage= GetInstance(ActivevoicesSynthesisRequestsPage.class);
			CurrentPage.As(ActivevoicesSynthesisRequestsPage.class).getReady(test, logger,accountName, chosenProject , chosenDomain, con, true);

			// Click the play button
			CurrentPage.As(HomePage.class).btnplay.click();
			
			// Open the other windows and login
			for (int i = 1; i < numberOfRuns; i++) {
				
	             // Open new window
				//((JavascriptExecutor)DriverContext._Driver).executeScript("window.open();");

				// Focus on last opened window
//				for (String handle : DriverContext._Driver.getWindowHandles()) {
//					DriverContext._Driver.switchTo().window(handle);
//					}
		
				CurrentPage= GetInstance(ActivevoicesSynthesisRequestsPage.class);
				CurrentPage.As(ActivevoicesSynthesisRequestsPage.class).getReadyForRepeatDuplicate(test, logger, isThereMoreThanOneAssignedDomain);
				
				// Click the play button
				CurrentPage.As(HomePage.class).btnplay.click();
				CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				CurrentPage.As(HomePage.class).checkPauseAfterSynthesizing();
			}
			
			
//			// Click play in all the buttons
//			for (String handle : DriverContext._Driver.getWindowHandles()) {
//				DriverContext._Driver.switchTo().window(handle);
//				
//				// Click the play button
//				CurrentPage.As(HomePage.class).btnplay.click();
//				
//				}
			
			int failed = 0;
			// Check for error synthiszing messages
			for (String handle : DriverContext._Driver.getWindowHandles()) {
				DriverContext._Driver.switchTo().window(handle);
			}
				// Check the messages
				try {
					//CurrentPage.As(HomePage.class).fadedPopup(logger, "you have reached  maximum request this hour.  try in the next hour.");
					
					////CurrentPage.As(HomePage.class).isHomepageReady();
					String message = CurrentPage.As(HomePage.class).displayedMessage.getText();
					boolean MaxLimit=message.equals("you have reached maximum request this hour. try in the next hour.     X");
					if (!message.isEmpty() && message.equals("you have reached maximum request this hour. try in the next hour.     X"))
						failed ++;
			    	assertTrue(MaxLimit);

				} catch (Exception e) {
					System.out.println(e);
				}
								
				
	
			int result = numberOfRuns-failed;
			LogUltility.log(test, logger, "Succeded to synthesize: " + result);
	}
		
		/**
		 *  SRA-148:Check the active voices
		 * @throws InterruptedException
		 * @throws SQLException 
		 * @throws FileNotFoundException 
		 */
		@Test (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
		public void SRA_148_Check_the_active_voices() throws InterruptedException, SQLException, FileNotFoundException
		{   
			test = extent.createTest("ActivevoicesSynthesisRequests-SRA-148", "Check the active voices");
			
			int failedVoices = 0;
			ArrayList<String> failedVoicesArr = new ArrayList<String>();
		    HashMap voicesTimes = new HashMap(); 
			
			DriverContext.browser.GoToUrl(Setting.AUT_URL);
			
//			LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(HomePage.class), logger);
//			CurrentPage= GetInstance(HomePage.class);
//			CurrentPage.As(HomePage.class).isHomepageReady();
			
			CurrentPage= GetInstance(ActivevoicesSynthesisRequestsPage.class);
			CurrentPage.As(ActivevoicesSynthesisRequestsPage.class).getReady(test, logger,accountName, chosenProject , chosenDomain, con, true);
			
			// Get list of all voices
			List<String> voiceValues = CurrentPage.As(HomePage.class).getVoiceValues();
			voiceValues.remove(0);
			
			System.out.println("Voices to check: " + voiceValues);
			
			// Go over all the available voices and synthesize text with them
			for (String voice : voiceValues) {
				
				DriverContext._Driver.navigate().refresh();
				CurrentPage.As(HomePage.class).isHomepageReady();
				
				// Choose a voice from the list and a random domain
				CurrentPage.As(HomePage.class).cmbVoice.click();
				CurrentPage.As(HomePage.class).Select_Item_In_ListBox(CurrentPage.As(HomePage.class).cmbVoice, voice);
				
//				String chosenDomain = CurrentPage.As(HomePage.class).chooseRandomDomain(con);
				
//				LogUltility.log(test, logger, "Chosen Voice: " + voice + ", Chosen Domain: " + chosenDomain);
				LogUltility.log(test, logger, "Chosen Voice: " + voice);
				
				// Enter text in the text box
//				String randomText = generateInput.RandomString(5, "words");
				String randomText = RandomText.getSentencesFromFile();
				
				LogUltility.log(test, logger, "Text to enter: " + randomText);
				CurrentPage.As(HomePage.class).Textinput.click();
				Thread.sleep(1000);
				CurrentPage.As(HomePage.class).Textinput.sendKeys(randomText);
				
				// Click the play button
				long startTime = System.currentTimeMillis();
				CurrentPage.As(HomePage.class).btnplay.click();
				
				// Wait for the test to start playing
				double synthesizeTime= CurrentPage.As(HomePage.class).finishedSynthsize(test, logger);
				long endTime = System.currentTimeMillis();
				
				try {
					// Check if the wave bar for clip is displayed
					boolean isWaveBarDisplayed = CurrentPage.As(HomePage.class).isWaveBarDisplayed();
					LogUltility.log(test, logger, "Is wave bar displayed: " + isWaveBarDisplayed);
					assertTrue(isWaveBarDisplayed);
					voicesTimes.put(voice, (endTime - startTime) / 1000);
				} catch (Exception e) {
					failedVoices++;
					failedVoicesArr.add(voice);
				}
			}	
			System.out.println("finished");
			if (failedVoices == 0) {
				LogUltility.log(test, logger, "Test Case PASSED");
				LogUltility.log(test, logger,"Result: " + voicesTimes);
			}
			else {
				LogUltility.log(test, logger,"Number of failed voices: " + failedVoices);
				LogUltility.log(test, logger,"These voices failed to synthesize: " + failedVoicesArr);
				assertTrue(false);
			}
		}

		@AfterMethod(alwaysRun = true)
	    public void getResult(ITestResult result, Method method) throws IOException
	    {

			int testcaseID = 0;
			boolean writeToReport = false;
			if (enableReportTestlink == true)
				testcaseID=rpTestLink.GetTestCaseIDByName("Active voices and Synthesis Requests",method.getName());
			System.out.println("tryCount: " + tryCount);
			if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
				writeToReport = true;
			}
			else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
				extent.removeTest(test);
				}
				else {
					writeToReport = true;
					}
			
			// Write to report
			if (writeToReport) {
				tryCount = 0;

				 // Write to report
				 if(result.getStatus() == ITestResult.FAILURE)
				    {
				        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
				        test.fail(result.getThrowable());
				        String screenShotPath = Screenshot.captureScreenShot();
				        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
				        // Send result to testlink
				        if (enableReportTestlink == true){
				        	try {
				        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
				            	LogUltility.log(test, logger, "Report to testtlink: " + response);
							} catch (Exception e) {
								LogUltility.log(test, logger, "Testlink Error: " + e);
							}
				        	}
				    }
				    else if(result.getStatus() == ITestResult.SUCCESS)
				    {
				        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
				        // Send result to testlink
				        if (enableReportTestlink == true){
				        	try {
				        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
				            	LogUltility.log(test, logger, "Report to testtlink: " + response);
				    		} catch (Exception e) {
				    			LogUltility.log(test, logger, "Testlink Error: " + e);
				    		}
				        	
				        	}
				    }
				    else
				    {
				        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
				        test.skip(result.getThrowable());
				    }
			        // Get console report
			        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
				    extent.flush();
			}
	    	}
		
			/**
			 * Closing the browser after running all the TCs
			 */
			@AfterClass(alwaysRun = true) 
			public void CloseBrowser() {
				 DriverContext._Driver.quit();
		    }
	}
