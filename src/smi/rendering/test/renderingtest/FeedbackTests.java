package smi.rendering.test.renderingtest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;

import jxl.read.biff.BiffException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.smi.framework.base.DriverContext;
import com.smi.framework.base.FrameworkInitialize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import smi.rendering.test.pages.FeedbackPage;
import smi.rendering.test.pages.HomePage;
import smi.rendering.test.pages.LoginPage;
import smi.rendering.test.pages.SelectPage;

import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;
import com.smi.framework.uiltilies.ConsoleReport;
import com.smi.framework.uiltilies.DatadUltilities;
import com.smi.framework.uiltilies.ExcelUltility;
import com.smi.framework.uiltilies.LogUltility;
import com.smi.framework.uiltilies.LoginUtilities;
import com.smi.framework.uiltilies.RandomText;
import com.smi.framework.uiltilies.ReportTestLink;
import com.smi.framework.uiltilies.ReportUtilities;
import com.smi.framework.uiltilies.RetryAnalyzer;
import com.smi.framework.uiltilies.Screenshot;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * All tests in Feedback folder
 *
 */
public class FeedbackTests extends FrameworkInitialize{
	
	private static Logger logger = null;
	private static Connection con;
	private static ExtentReports extent;
	private static ExtentTest test;
	public static ReportTestLink rpTestLink;
	private static boolean enableReportTestlink;
	private int tryCount = 0;
	private static RandomText generateInput;
	public boolean isThereMoreThanOneAssignedDomain;
	
	/**
	 * Initialize - run once before all tests
	 * @param browser
	 * @throws BiffException
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	@SuppressWarnings("deprecation")
	@Parameters({"testlink"})
	@BeforeClass(alwaysRun = true)
	public void Initialize(boolean testlink) throws BiffException, IOException, InterruptedException, ClassNotFoundException, SQLException {
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_NONE);
		ConfigReader.GetAllConfigVariable();
	
	// Initialize the report
	//@SuppressWarnings("unused")
	extent = ReportUtilities.report("Feedback", Setting.Browser);
	    
	// Logger
	logger = LogManager.getLogger(FeedbackTests.class);
	logger.info("--------------------------------------------------------------------------------------------------------");
	logger.info("Start a new Test suite= FeedbackTests ");
	logger.info("Feedback Tests - FrameworkInitilize");
	con=DatadUltilities.Connect_DB(Setting.DB_Host,Setting.DB_Name);
	logger.info("Connect to DB " + con.toString());
	
	InitializeBrowser(Setting.Browser);
	DriverContext.browser.GoToUrl(Setting.AUT_URL);
	
	// Initialize console report variables
	ConsoleReport.initializeConsoleReport();
	
	// Get account,project and domain with the requested language and logged-in email
	ExcelUltility exelul= new ExcelUltility(Setting.ExcelSheetData);
	List<ArrayList<String>> account_project_domain = GetInstance(SelectPage.class).As(SelectPage.class).getAccountProjectDomainComb(con, ExcelUltility.ReadCell("Email",1));
	LoginUtilities.login(GetInstance(LoginPage.class), GetInstance(SelectPage.class), GetInstance(HomePage.class), logger, true);
	Random random = new Random();
	int index = random.nextInt(account_project_domain.size());
	LoginUtilities.selectSpecificAccountProjectDomain(GetInstance(SelectPage.class), logger,account_project_domain.get(index).get(0),account_project_domain.get(index).get(1),account_project_domain.get(index).get(2));

	CurrentPage= GetInstance(HomePage.class);
	
	// Implicit wait
	DriverContext._Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
	// Report to testlink
	try {
		if (Setting.ReportToTestlink.equals("true") && testlink == true) {
			enableReportTestlink = true;
			rpTestLink=new ReportTestLink();
		}
		else enableReportTestlink = false;
	} catch (Exception e) {
		LogUltility.log(test, logger, "TESTLINK ERROR - COULD NOT CONNECT: " + e);
		enableReportTestlink = false;
	}
	
	// String builder in order to send the steps to testlink
	LogUltility.newSB();
	
	// Get the Records list
	generateInput = new RandomText();
	}
	
	/**
	 * Run this before each test
	 * @param method for test information
	 * @throws Exception
	 */
	@BeforeMethod(alwaysRun = true)
	 public void beforeTest(Method method) throws Exception {  
	  logger.info("");
	  logger.info("#####################################################");
	  logger.info("Starting Test: " + method.getName());
  	
	 CurrentPage = GetInstance(HomePage.class);
	 //Thread.sleep(5000);
	 CurrentPage.As(HomePage.class).isHomepageReady();
	 
		// Reset string builder
		LogUltility.sbClean();
		
		// Count how many a test was processed
		tryCount++;
     }

	/**
	 * SRA-96:Verify user can send feedback successfully
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test (groups="Full regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA96_Verify_user_can_send_Feedback_successfully() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("FeedbackTests-SRA-96" ,"Verify user can send feedback successfully");

		// Scroll Down
		CurrentPage.As(HomePage.class).scrollDown();
		
		// Open the feedback window
		LogUltility.log(test, logger, "Click the feedback link in the home page");
    	CurrentPage.As(HomePage.class).Feedback.click();
    	//Thread.sleep(3000);
    	
    	// Fill email and text
    	CurrentPage = GetInstance(FeedbackPage.class);
    	String randomEmail = generateInput.RandomString(5, "letters") + "@gmail.com";
//    	String randomText = generateInput.RandomString(5, "letters") + generateInput.RandomString(5, "letters") + generateInput.RandomString(5, "letters");
    	String randomText = RandomText.getSentencesFromFile();
    	LogUltility.log(test, logger, "Send Feedback with valid email and text - Random email : " + randomEmail + ", Random text: " + randomText);
    	CurrentPage.As(FeedbackPage.class).Send_Feedback(randomEmail, randomText);
    	
    	// Check the confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Your feedback has been submitted     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-97:Check feedback saved in the DB
	 * @throws InterruptedException
	 * @throws SQLException
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA97_Check_Feedback_Saved_to_DB() throws InterruptedException, SQLException, FileNotFoundException
	{
		test = extent.createTest("FeedbackTests-SRA-97" ,"Check feedback saved in the DB");
		
		// Scroll Down
		CurrentPage.As(HomePage.class).scrollDown();
				
		// Open the feedback window
		LogUltility.log(test, logger, "Click the feedback link in the home page " );
    	CurrentPage.As(HomePage.class).Feedback.click();
    	//Thread.sleep(3000);
    	
    	// Fill email and text
    	CurrentPage = GetInstance(FeedbackPage.class);
    	String randomEmail = generateInput.RandomString(5, "letters") + "@gmail.com";
//    	String randomText = generateInput.RandomString(5, "letters") + generateInput.RandomString(5, "letters") + generateInput.RandomString(5, "letters");
    	String randomText = RandomText.getSentencesFromFile();
    	LogUltility.log(test, logger, "Send Feedback with valid email and text - Random email : " + randomEmail + ", Random text: " + randomText);
    	CurrentPage.As(FeedbackPage.class).Send_Feedback(randomEmail, randomText);
    	
    	// Check the confirmation message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Your feedback has been submitted     X");
    	
    	// Verify that the feedback saved to DB
		CurrentPage = GetInstance(FeedbackPage.class);
    	List<List<String>> feedbackTable = CurrentPage.As(FeedbackPage.class).getFeedbackValues(con, randomEmail, randomText);
    	LogUltility.log(test, logger, "Results from DB: " + feedbackTable);
    	LogUltility.log(test, logger, "Text check: " + feedbackTable.get(1).get(1) + ", Email check: " + feedbackTable.get(1).get(0));
        assertEquals(randomText, feedbackTable.get(1).get(1));
        assertEquals(randomEmail, feedbackTable.get(1).get(0));
        LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-98:Verify user cannot send feedback without add text to the text box
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA98_Verify_user_cannot_send_Feedback_without_add_text_to_the_textbox() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("FeedbackTests-SRA-98" ,"Verify user cannot send feedback without add text to the text box");
		
		// Scroll Down
		CurrentPage.As(HomePage.class).scrollDown();
				
		// Open the feedback window
		LogUltility.log(test, logger, "Click the feedback link in the home page");
    	CurrentPage.As(HomePage.class).Feedback.click();
    	//Thread.sleep(3000);
    	
    	// Enter valid email and empty text
    	CurrentPage = GetInstance(FeedbackPage.class);
    	String randomEmail = generateInput.RandomString(5, "letters") + "@gmail.com";
    	String randomText = "";
    	LogUltility.log(test, logger, "Send Feedback with valid email and empty text - Random email : " + randomEmail + ", Random text: " + randomText);
    	CurrentPage.As(FeedbackPage.class).Send_Feedback(randomEmail, randomText);
    	
    	// Check the error message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please enter feedback text of at least 10 non-blank characters     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-99:Verify user cannot send feedback without email address
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA99_Verify_user_cannot_send_Feedback_without_Email_address() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("FeedbackTests-SRA-99" ,"Verify user cannot send feedback without email address");
		
		// Scroll Down
		CurrentPage.As(HomePage.class).scrollDown();
				
		// Open the feedback window
		LogUltility.log(test, logger, "Click the feedback link in the home page");
    	CurrentPage.As(HomePage.class).Feedback.click();
    	//Thread.sleep(3000);
    	
    	// Enter empty email and valid text
    	CurrentPage = GetInstance(FeedbackPage.class);
    	String randomEmail = "";
//    	String randomText = generateInput.RandomString(5, "letters") + generateInput.RandomString(5, "letters") + generateInput.RandomString(5, "letters");
    	String randomText = RandomText.getSentencesFromFile();
    	LogUltility.log(test, logger, "Send Feedback with empty email and valid text - Random email : " + randomEmail + ", Random text: " + randomText);
    	CurrentPage.As(FeedbackPage.class).Send_Feedback(randomEmail, randomText);
    	
    	// Check the error message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please enter email address     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-100:Verify user cannot send feedback with invalid email address
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA100_Verify_user_cannot_send_Feedback_with_invalid_Email_address() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("FeedbackTests-SRA-100" ,"Verify user cannot send feedback with invalid email address");
		
		// Scroll Down
		//CurrentPage.As(HomePage.class).scrollDown();
				
		// Open the feedback window
		LogUltility.log(test, logger, "Click the feedback link in the home page");
    	CurrentPage.As(HomePage.class).Feedback.click();
    	//Thread.sleep(3000);
    	
    	// Enter invalid email and valid text
    	CurrentPage = GetInstance(FeedbackPage.class);
    	String randomEmail = generateInput.RandomString(5, "letters");
//    	String randomText = generateInput.RandomString(5, "letters") + generateInput.RandomString(5, "letters") + generateInput.RandomString(5, "letters");
    	String randomText = RandomText.getSentencesFromFile();
    	LogUltility.log(test, logger, "Send Feedback with invalid email and a valid text - Random email : " + randomEmail + ", Random text: " + randomText);
    	CurrentPage.As(FeedbackPage.class).Send_Feedback(randomEmail, randomText);
    	
    	// Check the error message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please enter a valid email address     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	/**
	 * SRA-131:Verify the user gets popup when sending feedback with text less than 10 characters
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= {"Full Regression", "Smoke Test"}, retryAnalyzer = RetryAnalyzer.class)
	public void SRA131_Verify_user_gets_popup_when_sending_Feedback_with_text_less_than_10_characters() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("FeedbackTests-SRA-131" ,"Verify the user gets popup when sending feedback with text less than 10 characters");
		
		// Scroll Down
		CurrentPage.As(HomePage.class).scrollDown();
				
		// Open the feedback window
		LogUltility.log(test, logger, "Click the feedback link in the home page");
    	CurrentPage.As(HomePage.class).Feedback.click();
    	Thread.sleep(3000);
    	
    	// Enter valid email and short text
    	CurrentPage = GetInstance(FeedbackPage.class);
    	String randomEmail = generateInput.RandomString(5, "letters") + "@gmail.com";
    	String randomText = generateInput.RandomString(5, "letters");
    	LogUltility.log(test, logger, "Send Feedback with valid email and text less than 10 chars - Random email : " + randomEmail + ", Random text: " + randomText);
    	CurrentPage.As(FeedbackPage.class).Send_Feedback(randomEmail, randomText);
    	
    	// Check the error message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please enter feedback text of at least 10 non-blank characters     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}
	
	
	/**
	 * SRA-204:Verify that user can�t send blank feedback
	 * @throws InterruptedException
	 * @throws FileNotFoundException 
	 */
	@Test  (groups= "Full Regression", retryAnalyzer = RetryAnalyzer.class)
	public void SRA204_Verify_that_user_cant_send_blank_feedback() throws InterruptedException, FileNotFoundException
	{
		test = extent.createTest("FeedbackTests-SRA-204" ,"Verify that user can�t send blank feedback");
		
		// Scroll Down
		CurrentPage.As(HomePage.class).scrollDown();
				
		// Open the feedback window
		LogUltility.log(test, logger, "Click the feedback link in the home page");
    	CurrentPage.As(HomePage.class).Feedback.click();
    	//Thread.sleep(3000);
    	
    	// Enter valid email and empty text
    	CurrentPage = GetInstance(FeedbackPage.class);
    	String randomEmail = generateInput.RandomString(5, "letters") + "@gmail.com";
    	String randomText = "                   ";
    	LogUltility.log(test, logger, "Send Feedback with valid email and empty text - Random email : " + randomEmail + ", Random text: " + randomText);
    	CurrentPage.As(FeedbackPage.class).Send_Feedback(randomEmail, randomText);
    	
    	// Check the error message
		CurrentPage = GetInstance(HomePage.class);
		CurrentPage.As(HomePage.class).fadedPopup(logger, "Please enter feedback text of at least 10 non-blank characters     X");
		LogUltility.log(test, logger, "Test Case PASSED");
	}

	
	@AfterMethod(alwaysRun = true)
    public void getResult(ITestResult result, Method method) throws IOException
    {		
		int testcaseID = 0;
		boolean writeToReport = false;
		if (enableReportTestlink == true)
			testcaseID=rpTestLink.GetTestCaseIDByName("Feedback",method.getName());
		System.out.println("tryCount: " + tryCount);
		if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && Integer.parseInt(Setting.RetryFailed)==0) {
			writeToReport = true;
		}
		else if((result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) && tryCount < Integer.parseInt(Setting.RetryFailed)) {
			extent.removeTest(test);
			}
			else {
				writeToReport = true;
				}
		
		// Write to report
		if (writeToReport) {
			tryCount = 0;

			 // Write to report
			 if(result.getStatus() == ITestResult.FAILURE)
			    {
			        test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
			        test.fail(result.getThrowable());
			        String screenShotPath = Screenshot.captureScreenShot();
			        test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.FAILED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
						} catch (Exception e) {
							LogUltility.log(test, logger, "Testlink Error: " + e);
						}
			        	}
			    }
			    else if(result.getStatus() == ITestResult.SUCCESS)
			    {
			        test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test Case PASSED", ExtentColor.GREEN));
			        // Send result to testlink
			        if (enableReportTestlink == true){
			        	try {
			        		String response = rpTestLink.reportResult(testcaseID, LogUltility.sb.toString(), ExecutionStatus.PASSED);
			            	LogUltility.log(test, logger, "Report to testtlink: " + response);
			    		} catch (Exception e) {
			    			LogUltility.log(test, logger, "Testlink Error: " + e);
			    		}
			        	
			        	}
			    }
			    else
			    {
			        test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" Test Case SKIPPED", ExtentColor.ORANGE));
			        test.skip(result.getThrowable());
			    }
		        // Get console report
		        ConsoleReport.GetJSErrosLog(test,logger,method.getName());
			    extent.flush();
		}
		// Refresh the website for the new test
		DriverContext._Driver.navigate().refresh();
    	}
	
		/**
		 * Closing the browser after running all the TCs
		 */
		@AfterClass(alwaysRun = true) 
		public void CloseBrowser() {  
			 DriverContext._Driver.quit();
			}
    }
