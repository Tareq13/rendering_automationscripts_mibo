package com.smi.framework.base;

public enum BrowserType{
	
	FireFox,
	IE,
	Chrome,
	Safari,
	Edge,
	
}