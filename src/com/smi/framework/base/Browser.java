package com.smi.framework.base;

import org.openqa.selenium.WebDriver;

public class Browser {

	private WebDriver _driver;
	public BrowserType Type;
	
	public Browser(WebDriver driver) {
		
		this._driver=driver;
	}
	
	public void  GoToUrl(String Url){
		
		_driver.get(Url);
		
	}
	public void Maximize(){
		_driver.manage().window().maximize();
	}



}
