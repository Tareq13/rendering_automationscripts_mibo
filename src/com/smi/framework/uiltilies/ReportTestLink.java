package com.smi.framework.uiltilies;
import com.smi.framework.base.Base;
import com.smi.framework.config.ConfigReader;
import com.smi.framework.config.Setting;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.model.ReportTCResultResponse;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.model.TestProject;
import br.eti.kinoshita.testlinkjavaapi.model.TestSuite;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseDetails;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseStatus;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


public class ReportTestLink extends Base{

    public static String DEVKEY="852737ec74133c46ccb020cfb5d5bcbf";
    public static TestLinkAPI api;
 
     public ReportTestLink() throws MalformedURLException{
        
    	URL testLinkURL=new URL("http://10.1.10.12/testlink/lib/api/xmlrpc/v1/xmlrpc.php");
    	api= new TestLinkAPI(testLinkURL,DEVKEY);
    }

    public String reportResult(Integer testCaseId, String notes,ExecutionStatus status) throws TestLinkAPIException, IOException 
    {
    	Integer testPlanId = getTestPlan();
    	Integer buildId = getLastBuildIDInPlan();
    	String buildName = getLastBuildNameInPlan();
    	ReportTCResultResponse rs=api.setTestCaseExecutionResult(testCaseId, 0, testPlanId, status, buildId, buildName, notes, Boolean.TRUE, "",0, "", null, Boolean.TRUE);
    	return rs.getMessage();
    }
    
    public int GetTestCaseIDByName(String TestSuiteName,String TestCaseName)
    {
    	int testSuiteID = this.getSuiteIDByName(TestSuiteName);
    	TestCase[] tc= api.getTestCasesForTestSuite(testSuiteID,Boolean.TRUE,TestCaseDetails.SIMPLE);
		for (TestCase c : tc)
		{
			System.out.println(TestCaseName.split("\\_")[0]);
			System.out.println(c.getFullExternalId().toString().replaceAll("-",""));
			if(TestCaseName.split("\\_")[0].equals(c.getFullExternalId().toString().replaceAll("-","")))
			{
			    return c.getId(); 
			}
		}
		return 0;
    }
    
    public int getSuiteIDByName(String TestSuiteName) throws TestLinkAPIException
    {
    	TestSuite[] ts= api.getTestSuitesForTestPlan(4724);
    	for (TestSuite c : ts)
		{
			if(TestSuiteName.equals(c.getName()))
			{
			    return c.getId(); 
			}
		}
		return 0;
    }

	public int getTestPlan() throws IOException {
    	TestPlan testPlan = api.getTestPlanByName(Setting.TestLinkPlan, Setting.TestLinkProject);
    	return testPlan.getId();
	}

	public int getLastBuildIDInPlan() throws IOException {
		int testPlanId = getTestPlan();
		Build lastBuild = api.getLatestBuildForTestPlan(testPlanId);
		return lastBuild.getId();
	}
	
	public String getLastBuildNameInPlan() throws IOException {
		int testPlanId = getTestPlan();
		Build lastBuild = api.getLatestBuildForTestPlan(testPlanId);
		return lastBuild.getName();
	}

}
