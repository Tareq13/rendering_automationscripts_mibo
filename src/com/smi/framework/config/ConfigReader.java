package com.smi.framework.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.io.InputStream;

public class ConfigReader {

	
	Properties p;
	FileInputStream input;
	
	public static void GetAllConfigVariable() throws IOException
	{
	    ConfigReader confreader= new ConfigReader();
		confreader.ReadProperties();
		
	}
	
	public void ReadProperties() throws IOException
	{
		 p= System.getProperties();
		 input = new FileInputStream("src\\com\\smi\\framework\\config\\GlobalConfig.properties");

		  p.load(input);
		
		Setting.AUT_URL=p.getProperty("AUT_URL");
		Setting.UserName_DB=p.getProperty("UserName_DB");
		Setting.Password_DB=p.getProperty("Password_DB");
		Setting.LogPath=p.getProperty("LogPath");
		Setting.ExcelSheetData=p.getProperty("ExcelSheetData");
		Setting.DB_Host=p.getProperty("DB_Host");
		Setting.DB_Name=p.getProperty("DB_Name");
		Setting.ReportToTestlink=p.getProperty("ReportToTestlink");
		Setting.TestLinkProject=p.getProperty("TestLinkProject");
		Setting.TestLinkPlan=p.getProperty("TestLinkPlan");
		Setting.RetryFailed=p.getProperty("RetryFailed");
		Setting.Language=p.getProperty("Language");
		Setting.Browser=p.getProperty("Browser");
		
		/*
		Setting.AUT_URL="http://10.1.10.138:8080/RenderingApp";
		Setting.UserName_DB="vbuser";
		Setting.Password_DB="gfdsa12345!!";
		Setting.LogPath="log";
		Setting.ExcelSheetData="LoginUser.xls";
		Setting.DB_Name="voicebank";
		Setting.ReportToTestlink="true";
		Setting.TestLinkProject="Speech Rendering App";
		Setting.TestLinkPlan="AutomationRegression";
		
		*/
		
	}
}
